from django.contrib import admin

from models import (Gallery, GalleryImage, GalleryGroup, GalleryUser, Image,
                    Comment)


admin.site.register(Gallery)
admin.site.register(GalleryImage)
admin.site.register(Image)
admin.site.register(GalleryUser)
admin.site.register(GalleryGroup)
admin.site.register(Comment)
