from django.conf.urls import patterns, url

urlpatterns = patterns('gallery.gallery.views',
    url(r'^$', 'index', name='gallery_index'),
    url(r'^g/(?P<slug>[A-Za-z0-9\-_]+)/(?P<gallery_pk>\d+)$', 'gallery', name='gallery_gallery'),
    url(r'^api/image/(\d+)$', 'image_data', name='gallery_image_data'),
    url(r'^api/gallery/(\d+)$', 'gallery_data', name='gallery_gallery_data'),
    url(r'^api/gallery/(\d+)/cover$', 'gallery_cover', name='gallery_gallery_cover'),
    url(r'^api/comments/(\d+)/(\d+)$', 'comments_view', name='gallery_comments'),
    url(r'^media/(\d+)$', 'serve_image', name='gallery_serve_image'),
)
