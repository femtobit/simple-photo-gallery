from django.conf import settings
from django.db import transaction
from django.db.models import Count
from django.utils.text import slugify
from gi.repository import GExiv2 as gexiv2
import os
import PIL.Image
import PIL.ImageOps
import pyexiv2

import models

# Dictionary mapping supported PIL image format strings to mimetypes
FORMAT_TO_MIMETYPE = {
    'JPEG': 'image/jpeg',
}
# TODO: Support more image formats

def resize_image(image, size=(200,200), method='crop'):
    """Creates and returns a resized version of the given image.
    If the aspect ratio of the source image does not match the
    target size, there are two possibilities, depending on the
    method parameter:
        - scale: The aspect ratio of the source is changed,
                 distorting the image.
        - crop:  The source image is croped to match the
                 target aspect ratio.
        - preserve: The original aspect ratio is preserved
                    and the image is resized to be smaller than size.

    Arguments:
    image -- a PIL Image
    size -- a tuple of width and height of the thumbnail
    method -- one of 'scale', 'crop'
    """
    if not method in ('scale', 'crop', 'preserve'):
        raise ValueError("method must be one of 'scale', 'crop'")

    if method == 'scale':
        thumb = image.resize(size, PIL.Image.LANCZOS)
    elif method == 'crop':
        thumb = PIL.ImageOps.fit(image, size, PIL.Image.LANCZOS)
    elif method == 'preserve':
        thumb = image.copy()
        thumb.thumbnail(size, PIL.Image.LANCZOS)

    return thumb


def get_path(upload_path, tag, image, ext):
    path = os.path.join(upload_path, tag)
    try:
        os.makedirs(path)
    except OSError:
        pass
    path = os.path.join(path, str(image.pk))
    return "{}.{}".format(path, ext)


@transaction.atomic
def create_image_files(image, pil_image, upload_path):
    mimetype = FORMAT_TO_MIMETYPE[pil_image.format]

    # Save original size
    orig_file = models.ImageFile(
        image=image,
        width=pil_image.width,
        height=pil_image.height,
        size_class=models.IMAGE_SIZE_ORIGINAL,
        mimetype=mimetype,
    )
    path = get_path(upload_path, 'original', image,
                    models.MIMETYPE_TO_EXT[mimetype])
    orig_file.file_path = path
    pil_image.save(path, progressive=True, optimize=True)
    orig_file.save()

    # Create thumbnail
    for size_class in (models.IMAGE_SIZE_THUMBNAIL,
                       models.IMAGE_SIZE_SMALL,
                       models.IMAGE_SIZE_LARGE):
        size = models.IMAGE_SIZES[size_class]
        if isinstance(size, tuple):
            pil_resized = resize_image(pil_image, size, 'crop')
            tag = '{}x{}'.format(size[0], size[1])
        else:
            tag = str(size)
            if max(pil_image.size) <= size:
                pil_resized = pil_image
            else:
                pil_resized = resize_image(pil_image, (size, size), 'preserve')
        resized = models.ImageFile(
            image=image,
            width=pil_resized.width,
            height=pil_resized.height,
            size_class=size_class,
            mimetype=mimetype,
        )
        path = get_path(upload_path, tag, image,
                        models.MIMETYPE_TO_EXT[mimetype])
        resized.file_path = path
        pil_resized.save(path, progressive=True, optimize=True)
        resized.save()


@transaction.atomic
def import_image_from_file(filename, upload_path, creator):
    def _from_xmp_lang_alt(tag):
        lang_alt = tag.value
        if 'x-default' in lang_alt:
            return lang_alt['x-default']
        else: # return random value
            return lang_alt.values()[0]

    metadata = pyexiv2.ImageMetadata(filename)
    metadata.read()
    gexiv2_metadata = gexiv2.Metadata(filename)
    pil_image = PIL.Image.open(filename)

    if pil_image.format not in FORMAT_TO_MIMETYPE.keys():
        raise ValueError("Unsupported image format: {}".format(
            pil_image.format))
    
    # Create Image class
    image = models.Image(date=gexiv2_metadata.get_date_time())
    if 'Xmp.dc.title' in metadata:
        image.title = _from_xmp_lang_alt(metadata['Xmp.dc.title'])
    if 'Xmp.dc.description' in metadata:
        image.description = _from_xmp_lang_alt(metadata['Xmp.dc.description'])
    if creator:
        image.creator = creator
    elif 'Xmp.dc.creator' in metadata:
        image.creator = metadata['Xmp.dc.creator'].value[0]
    image.save()

    # Create sizes
    create_image_files(image, pil_image, upload_path)

    print("Imported: {}\n\tfrom {}".format(image, filename))
    return image


@transaction.atomic
def set_cover_image(gallery, image):
    if models.ImageFile.objects.filter(image=image,
                    size_class=models.IMAGE_SIZE_ALBUMCOVER).count() == 0:
        pil_image = PIL.Image.open(image.original_size.file_path)

        size = models.IMAGE_SIZES[models.IMAGE_SIZE_ALBUMCOVER]
        pil_resized = resize_image(pil_image, size, 'crop')

        resized = models.ImageFile(
            image=image,
            width=pil_resized.width,
            height=pil_resized.height,
            size_class=models.IMAGE_SIZE_ALBUMCOVER,
            mimetype=image.original_size.mimetype,
        )
        path = get_path(gallery.upload_path, 'cover', image,
                        models.MIMETYPE_TO_EXT[resized.mimetype])
        resized.file_path = path
        pil_resized.save(path, progressive=True, optimize=True)
        resized.save()

    gallery.cover_image = image
    gallery.save()


@transaction.atomic
def import_gallery(title,
                   image_filenames,
                   creator=None):
    gallery = models.Gallery(title=title,
                             slug=slugify(title),
                             access=models.ACCESS_PRIVATE)
    gallery.save()

    upload_path = os.path.join(settings.GALLERY_ROOT, str(gallery.pk))
    try:
        os.makedirs(upload_path)
    except OSError:
        pass
    gallery.upload_path = upload_path
    gallery.save()

    for i, filename in enumerate(image_filenames):
        image = import_image_from_file(filename, upload_path, creator)
        gi = models.GalleryImage(gallery=gallery, image=image, index=i+1)
        gi.save()

    first = models.GalleryImage.objects.get(gallery=gallery, index=1)
    set_cover_image(gallery, first.image)
    gallery.date = first.image.date
    gallery.save()


def orphaned_images():
    """Return query set of images that are not contained in any gallery.
    """
    return (models.Image.objects
                .annotate(num_galleries=Count('gallery'))
                .filter(num_galleries=0))


@transaction.atomic
def cleanup():
    """Remove orphaned images."""
    orphaned_images().delete()
    # TODO: Recursively delete empty directories below GALLERY_ROOT
    # TODO: Delete image sizes that are not used.
