from django.core.management.base import BaseCommand#, CommandError
from gallery.gallery.util import import_gallery

from gallery import settings


HELPTEXT = """Create a new gallery and import images files.

The images are added to the gallery in the order in which they are
passed as arguments. Title and description are read from the image
XMP metadata, more precisely from the Dublin Core tags
    Xmp.dc.title,
    Xmp.dc.description, and
    Xmp.dc.creator
in libexiv2 terms (see http://www.exiv2.org/tags-xmp-dc.html). These tags
can be set via photo management software (tested with digikam and
darktable). The title and description attributes support multiple
languages. The import uses only one of those languages: 'x-default'
if present and a random language otherwise. The creator attribute
supports multiple creators. We only import the first.

All required image sizes are created automatically, so you do not
need to resize the images before import, unless you want to reduce
the size of the album (since the original files are always stored).
"""


class Command(BaseCommand):
    help = HELPTEXT

    def add_arguments(self, parser):
        parser.add_argument('gallery_title', nargs=1)
        parser.add_argument('image_file', nargs='+')
        parser.add_argument('-T', '--time_zone', default=settings.TIME_ZONE,
                            help='Time zone used to interpret the image '
                                  'timestamps. [Default: {}]'.format(
                                        settings.TIME_ZONE))
        parser.add_argument('-c', '--creator',
                            help='Creator of the images. Overrides the '
                                 'values from the image tags.')

    def handle(self, *args, **options):
        import_gallery(options['gallery_title'][0], options['image_file'],
                       creator=options.get('creator', None))
