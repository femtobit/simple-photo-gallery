var pswpElement = document.querySelectorAll('.pswp')[0];

function openGallery(index) {
    var options = {
        galleryUID: GALLERY_ID,
        preload: [1,3],
        shareButtons: [
            {id:'download', label:'Download image', url:'{% templatetag openvariable %}raw_image_url{% templatetag closevariable %}', download:true}
        ],
        index: index-1
    };
    var gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, ITEMS, options);
    gallery.init();
}

function getHashParams() {
    var hashParams = {};
    var e,
        a = /\+/g,  // Regex for replacing addition symbol with a space
        r = /([^&;=]+)=?([^&;]*)/g,
        d = function (s) { return decodeURIComponent(s.replace(a, " ")); },
        q = window.location.hash.substring(1);

    while (e = r.exec(q))
       hashParams[d(e[1])] = d(e[2]);

    return hashParams;
}

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

function preloadImage(url) {
    var img = new Image();
    img.src = url;
}

function getApiCommentsUrl(image_id) {
    return '/api/comments/' + GALLERY_ID + '/' + image_id;
}

function getComments(image_id, success_callback) {
    return $.getJSON(getApiCommentsUrl(image_id), {}, success_callback);
}

function postComment(image_id, author, text, success_callback) {
    return $.post(getApiCommentsUrl(image_id), {
        text: text,
        author: author,
    }, success_callback);
}

function setCoverImage(image_id, success_callback) {
    return $.post(API_SET_COVER_URL, {
        image_id: image_id,
    }, success_callback);
}

$(function() {
    CSRF_TOKEN = getCookie('csrftoken');

    $.ajaxPrefilter(function(options, originalOptions, jqXHR) {
        if (options.type.toLowerCase() === "post") {
            jqXHR.setRequestHeader('X-CSRFToken', CSRF_TOKEN);
        }
    });

    $.getJSON(API_GALLERY_DATA_URL, function(data) {
        GALLERY_DATA = data;
        ITEMS = $.map(data.images, function(image) {
            var thumb = image.sizes.thumbnail;
            var large = image.sizes.large;
            preloadImage(thumb.url);
            return {
                title: image.title,
                msrc: thumb.url,
                src: large.url,
                w: large.width,
                h: large.height,
            };
        });

        var params = getHashParams();
        if('pid' in params) {
            openGallery(parseInt(params.pid));
        }
    });
});
