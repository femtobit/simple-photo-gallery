# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gallery', '0003_auto_20150823_1653'),
    ]

    operations = [
        migrations.AddField(
            model_name='gallery',
            name='date',
            field=models.DateTimeField(null=True, blank=True),
        ),
    ]
