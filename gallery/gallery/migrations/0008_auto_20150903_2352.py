# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('gallery', '0007_gallery_slug'),
    ]

    operations = [
        migrations.CreateModel(
            name='Comment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('text', models.TextField()),
                ('date', models.DateTimeField(auto_now=True)),
                ('author', models.CharField(max_length=200)),
                ('gallery_image', models.ForeignKey(to='gallery.GalleryImage')),
                ('user', models.ForeignKey(blank=True, to=settings.AUTH_USER_MODEL, null=True)),
            ],
        ),
        migrations.AlterField(
            model_name='imagefile',
            name='size_class',
            field=models.CharField(max_length=1, choices=[(b't', b'thumbnail'), (b's', b'small'), (b'a', b'albumcover'), (b'l', b'large'), (b'o', b'original')]),
        ),
    ]
