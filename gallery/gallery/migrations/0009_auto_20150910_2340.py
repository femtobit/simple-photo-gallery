# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gallery', '0008_auto_20150903_2352'),
    ]

    operations = [
        migrations.CreateModel(
            name='GalleryCollection',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=200)),
                ('date', models.DateTimeField(null=True, blank=True)),
            ],
        ),
        migrations.AddField(
            model_name='gallery',
            name='description',
            field=models.TextField(blank=True),
        ),
        migrations.AddField(
            model_name='gallery',
            name='extra_data',
            field=models.TextField(blank=True),
        ),
        migrations.AddField(
            model_name='gallerycollection',
            name='galleries',
            field=models.ManyToManyField(to='gallery.Gallery'),
        ),
    ]
