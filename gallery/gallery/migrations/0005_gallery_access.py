# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gallery', '0004_gallery_date'),
    ]

    operations = [
        migrations.AddField(
            model_name='gallery',
            name='access',
            field=models.IntegerField(default=0, choices=[(0, b'public'), (1, b'private')]),
        ),
    ]
