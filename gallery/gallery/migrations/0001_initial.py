# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Gallery',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=200)),
                ('upload_path', models.FilePathField(recursive=True, allow_files=False, allow_folders=True, blank=True, path=b'/home/damian/devel/projects/gallery/media/gallery/images', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='GalleryImage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('index', models.IntegerField()),
                ('gallery', models.ForeignKey(to='gallery.Gallery')),
            ],
        ),
        migrations.CreateModel(
            name='Image',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=200, blank=True)),
                ('description', models.TextField(blank=True)),
                ('date', models.DateTimeField(null=True, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='ImageFile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('file_path', models.FilePathField(path=b'/home/damian/devel/projects/gallery/media/gallery/images', recursive=True)),
                ('size_class', models.CharField(max_length=1, choices=[(b't', b'thumbnail'), (b'l', b'large'), (b'o', b'original')])),
                ('width', models.IntegerField()),
                ('height', models.IntegerField()),
                ('mimetype', models.CharField(max_length=20)),
                ('last_modified', models.DateTimeField(auto_now=True)),
                ('image', models.ForeignKey(to='gallery.Image')),
            ],
        ),
        migrations.AddField(
            model_name='galleryimage',
            name='image',
            field=models.ForeignKey(to='gallery.Image'),
        ),
        migrations.AddField(
            model_name='gallery',
            name='cover_image',
            field=models.ForeignKey(related_name='+', blank=True, to='gallery.Image', null=True),
        ),
        migrations.AddField(
            model_name='gallery',
            name='images',
            field=models.ManyToManyField(to='gallery.Image', through='gallery.GalleryImage'),
        ),
    ]
