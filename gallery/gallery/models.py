from binascii import hexlify
from django.conf import settings
from django.contrib.auth.models import Group, User
from django.db import models
from django.db.models.signals import post_delete
from django.dispatch import receiver
import os

from util import cleanup

MIMETYPE_TO_EXT = {
    'image/jpeg': 'jpg',
}

IMAGE_SIZE_THUMBNAIL = 't'
IMAGE_SIZE_SMALL = 's'
IMAGE_SIZE_ALBUMCOVER = 'a'
IMAGE_SIZE_LARGE = 'l'
IMAGE_SIZE_ORIGINAL = 'o'
# Dict of image sizes.
IMAGE_SIZES = {
    IMAGE_SIZE_THUMBNAIL: 200,
    IMAGE_SIZE_SMALL: (300,300),
    IMAGE_SIZE_ALBUMCOVER: (600,400),
    IMAGE_SIZE_LARGE: 1600,
}


def create_image_filename(instance, filename):
    return "gallery/image/%Y/%m/{}.{}x{}.{}".format(
        hexlify(os.urandom(32)),
        instance.width,
        instance.height,
        MIMETYPE_TO_EXT[instance.mimetype],
    )


class ImageFile(models.Model):
    IMAGE_SIZE_CHOICES = [
        (IMAGE_SIZE_THUMBNAIL, 'thumbnail'),
        (IMAGE_SIZE_SMALL, 'small'),
        (IMAGE_SIZE_ALBUMCOVER, 'albumcover'),
        (IMAGE_SIZE_LARGE, 'large'),
        (IMAGE_SIZE_ORIGINAL, 'original'),
    ]

    image = models.ForeignKey('Image')
    file_path = models.FilePathField(path=settings.GALLERY_ROOT,
                                     recursive=True)
    size_class = models.CharField(max_length=1,
                                  choices=IMAGE_SIZE_CHOICES)
    width = models.IntegerField()
    height = models.IntegerField()
    mimetype = models.CharField(max_length=20)
    last_modified = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return "{} ({}, {}:{}x{})".format(
            os.path.relpath(self.file_path, settings.GALLERY_ROOT),
            self.mimetype,
            self.size_class,
            self.width,
            self.height,
        )


class Image(models.Model):
    title = models.CharField(max_length=200, blank=True)
    description = models.TextField(blank=True)
    creator = models.TextField(blank=True)
    date = models.DateTimeField(null=True, blank=True)

    def get_size(self, size_class):
        return ImageFile.objects.get(image=self,
                                     size_class=size_class)

    @property
    def thumbnail_size(self):
        return self.get_size(IMAGE_SIZE_THUMBNAIL)

    @property
    def small_size(self):
        return self.get_size(IMAGE_SIZE_SMALL)

    @property
    def album_cover_size(self):
        return self.get_size(IMAGE_SIZE_ALBUMCOVER)

    @property
    def large_size(self):
        return self.get_size(IMAGE_SIZE_LARGE)

    @property
    def original_size(self):
        return self.get_size(IMAGE_SIZE_ORIGINAL)

    def user_can_view(self, user):
        gis = GalleryImage.objects.filter(image=self)
        for gi in gis:
            if gi.gallery.user_can_view(user):
                return True
        return False

    def user_can_edit(self, user):
        gis = GalleryImage.objects.filter(image=self)
        for gi in gis:
            if gi.gallery.user_can_edit(user):
                return True
        return False

    def __unicode__(self):
        s = str(self.pk)
        if self.title:
            s += ' "{}"'.format(self.title)
        else:
            s += ' (no title)'
        if self.date:
            s += ' {}'.format(self.date)
        return s


ACCESS_PUBLIC = 0
ACCESS_PRIVATE = 1
ACCESS_CHOICES = [
    (ACCESS_PUBLIC, 'public'),
    (ACCESS_PRIVATE, 'private'),
]
class Gallery(models.Model):
    title = models.CharField(max_length=200)
    slug = models.CharField(max_length=200)
    images = models.ManyToManyField(Image, through='GalleryImage')
    upload_path = models.FilePathField(path=settings.GALLERY_ROOT,
                                       recursive=True,
                                       allow_files=False,
                                       allow_folders=True,
                                       null=True, blank=True)
    cover_image = models.ForeignKey(Image, null=True, blank=True,
                                    related_name='+')
    date = models.DateTimeField(null=True, blank=True)
    access = models.IntegerField(choices=ACCESS_CHOICES,
                                 default=0)
    description = models.TextField(blank=True)
    # extra data in json format
    extra_data = models.TextField(blank=True)

    def _check_permission(self, user, permission):
        if not user.is_authenticated():
            return False

        if GalleryUser.objects.filter(gallery=self,
                                      user=user,
                                      permission=permission).count() > 0:
            return True

        ggs = GalleryGroup.objects.filter(gallery=self,
                                          permission=permission)
        for gg in ggs:
            if gg.group.user_set.filter(pk=user.pk).count() > 0:
                return True

        return False

    def user_can_view(self, user):
        if self.access == ACCESS_PUBLIC:
            return True
        return self._check_permission(user, PERMISSION_VIEW)

    def user_can_edit(self, user):
        return self._check_permission(user, PERMISSION_EDIT)

    def user_can_comment(self, user):
        if not user.is_authenticated():
            return settings.ALLOW_UNAUTHENTICATED_COMMENTS
        else:
            return self.user_can_view(user)

    def image_count(self):
        return self.images.count()

    def __unicode__(self):
        return '"{}" ({} images, {})'.format(
            self.title, self.images.count(), self.get_access_display())


class GalleryImage(models.Model):
    gallery = models.ForeignKey(Gallery)
    image = models.ForeignKey(Image)
    index = models.IntegerField()

    def __unicode__(self):
        return '#{} in "{}"'.format(self.index, self.gallery)


class GalleryCollection(models.Model):
    title = models.CharField(max_length=200)
    date = models.DateTimeField(null=True, blank=True)
    galleries = models.ManyToManyField(Gallery)

    def __unicode__(self):
        return '"{}" ({} galleries)'.format(self.title, self.galleries.count())


class Comment(models.Model):
    gallery_image = models.ForeignKey(GalleryImage)
    text = models.TextField(blank=False)
    date = models.DateTimeField(auto_now=True)
    user = models.ForeignKey(User, null=True, blank=True)
    author = models.CharField(max_length=200)

    @property
    def author_name(self):
        if self.user and self.user.username:
            return self.user.username
        else:
            return self.author

    def __unicode__(self):
        return '{} at {} on {}'.format(self.author_name,
                                       self.date,
                                       self.gallery_image)


# Permissions:

PERMISSION_VIEW = 0
PERMISSION_EDIT = 1
PERMISSION_CHOICES = (
    (PERMISSION_VIEW, 'view'),
    (PERMISSION_EDIT, 'edit'),
)


class GalleryGroup(models.Model):
    gallery = models.ForeignKey(Gallery)
    group = models.ForeignKey(Group)
    permission = models.IntegerField(choices=PERMISSION_CHOICES)

    def __unicode__(self):
        return "GROUP {} CAN {} GALLERY {}".format(
            self.group, self.get_permission_display(), self.gallery)


class GalleryUser(models.Model):
    gallery = models.ForeignKey(Gallery)
    user = models.ForeignKey(User)
    permission = models.IntegerField(choices=PERMISSION_CHOICES)

    def __unicode__(self):
        return "USER {} CAN {} GALLERY {}".format(
            self.user, self.get_permission_display(), self.gallery)


@receiver(post_delete)
def post_delete_handler(sender, instance, **kwargs):
    if sender == ImageFile:
        os.remove(instance.file_path)
    elif sender == Gallery:
        cleanup()
