from django.conf import settings
from django.core.urlresolvers import reverse
from django.contrib.auth.views import redirect_to_login
from django.http import FileResponse, Http404, HttpResponse, JsonResponse
from django.shortcuts import get_object_or_404, redirect, render
from django.views.decorators.http import require_http_methods
import os

from models import Comment, Gallery, GalleryImage, Image, ImageFile
import util


def _get_image_data(image):
    return {
        'id': image.pk,
        'title': image.title,
        'description': image.description,
        'creator': image.creator,
        'date': image.date,
        'sizes': {
            imf.get_size_class_display(): {
                'url': reverse('gallery_serve_image', args=[imf.pk]),
                'width': imf.width,
                'height': imf.height,
            }
            for imf in image.imagefile_set.all()
        },
    }


def _get_gallery_data(gallery):
    gallery_images = GalleryImage.objects.filter(gallery=gallery)\
                                         .order_by('index')
    return {
        'id': gallery.pk,
        'title': gallery.title,
        'date': gallery.date,
        'images': {
            gi.index: _get_image_data(gi.image)
            for gi in gallery_images
        }
    }


def _get_comment_data(gallery_image):
    comments = Comment.objects.filter(gallery_image=gallery_image)\
                              .order_by('date')
    return [{
        'id': comment.pk,
        'author': comment.author_name,
        'date': comment.date,
        'text': comment.text,
    } for comment in comments]


def index(request):
    galleries = Gallery.objects.all().order_by('-date')
    galleries = [g for g in galleries if g.user_can_view(request.user)]
    return render(request, 'gallery/index.html', {
        'galleries': galleries,
    })


def gallery(request, slug, gallery_pk):
    gallery = get_object_or_404(Gallery, pk=gallery_pk)
    if not gallery.user_can_view(request.user):
        if not request.user.is_authenticated():
            return redirect_to_login(request.url)
        else:
            raise Http404('No Gallery matches the given query.')
    if slug != gallery.slug:
        return redirect('gallery_gallery', gallery.slug, gallery.pk,
                        permanent=True)
    gallery_images = GalleryImage.objects.filter(gallery=gallery)
    return render(request, 'gallery/gallery.html', {
        'gallery': gallery,
        'gallery_images': gallery_images,
    })


def gallery_data(request, gallery_pk):
    gallery = get_object_or_404(Gallery, pk=gallery_pk)
    if not gallery.user_can_view(request.user):
        raise Http404('No Gallery matches the given query.')
    data = _get_gallery_data(gallery)
    return JsonResponse(data)


def image_data(request, image_pk):
    image = get_object_or_404(Image, pk=image_pk)
    if not image.user_can_view(request.user):
        raise Http404('No Image matches the given query.')
    data = _get_image_data(image)
    return JsonResponse(data)


def serve_image(request, imagefile_pk):
    imagefile = get_object_or_404(ImageFile, pk=imagefile_pk)
    if not imagefile.image.user_can_view(request.user):
        raise Http404('No ImageFile matches the given query.')

    if settings.DEBUG:
        response = FileResponse(open(imagefile.file_path))
    elif settings.USE_NGINX_X_ACCEL_REDIRECT:
        response = HttpResponse()
        response['X-Accel-Redirect'] = (settings.NGINX_X_ACCEL_REDIRECT_LOCATION
            + '/' + os.path.relpath(imagefile.file_path, settings.GALLERY_ROOT))
    else:
        response = HttpResponse()
        response['X-Sendfile'] = imagefile.file_path
    
    response['Content-Type'] = imagefile.mimetype
    return response


def _create_comment(request, gi):
    errors = {}

    if not 'text' in request.POST or not request.POST['text'].strip():
        errors['text'] = 'This field is required.'
    else:
        text = request.POST['text'].strip()

    if not 'author' in request.POST or not request.POST['author'].strip():
        errors['author'] = 'This field is required.'
    else:
        author = request.POST['author'].strip()

    if errors:
        return JsonResponse({
            'errors': errors,
        }, status=400)

    if request.user.is_authenticated():
        user = request.user
    else:
        user = None

    comment = Comment(gallery_image=gi,
                      author=author,
                      user=user,
                      text=text)
    comment.save()
    return HttpResponse(status=200)


def _list_comments(request, gi):
    data = _get_comment_data(gi)
    return JsonResponse({
        'comments': data,
    })


@require_http_methods(['GET', 'POST'])
def comments_view(request, gallery_pk, image_pk):
    gi = get_object_or_404(GalleryImage,
                           gallery=gallery_pk,
                           image=image_pk)
    if not gi.gallery.user_can_comment(request.user):
        return HttpResponse(status=403)

    if request.method == 'GET':
        return _list_comments(request, gi)
    elif request.method == 'POST':
        return _create_comment(request, gi)


@require_http_methods(['POST'])
def gallery_cover(request, gallery_pk):
    gallery = get_object_or_404(Gallery, pk=gallery_pk)
    if not gallery.user_can_edit(request.user):
        return HttpResponse(status=403)
    
    errors = {}
    if not 'image_id' in request.POST or not request.POST['image_id'].strip():
        errors['image_id'] = 'This field is required.'

    image = get_object_or_404(Image, pk=request.POST['image_id'])
    if not image.user_can_edit(request.user):
        return HttpResponse(status=403)

    if errors:
        return JsonResponse({
            'errors': errors,
        }, status=400)
    
    util.set_cover_image(gallery, image)
    return HttpResponse(status=200)
