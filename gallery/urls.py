from django.conf import settings
from django.conf.urls import patterns, url, include
from django.contrib import admin
from django.contrib.auth import views as auth_views


urlpatterns = patterns('',
   url(r'^', include('gallery.gallery.urls')),
   url(r'^admin/', include(admin.site.urls)),
   url(r'^login/', auth_views.login, name='auth_login'),
   url(r'^logout/', auth_views.logout,
      {'next_page': settings.LOGIN_REDIRECT_URL},
      name='auth_logout'),
)
