Simple Photo Gallery
====================

This is a very simple web photo gallery written in Python using the Django web framework.
Images can be imported from the command line with the Django management command `import_images`. Example:
```
  $ ./manage.py import_images "Gallery Name" /path/to/images/*.jpg
```
For details, refer to the command's help message (`./manage.py import_images --help`).
Thumbnails and differently sized versions of the image are generated on import.

Currently, management of galleries and tasks such as setting the gallery cover image and date or deleting galleries can only be done via the Django management shell (`./manage.py shell`). Patches and pull requests adding management features to the web interface (for users having the `edit` permission on a gallery, see `models.py`) and/or the command line are welcome.

## Development ##

To setup the development environment, run
```
  $ virtualenv -p python2 venv
  $ source venv/bin/activate
  $ pip install -r requirements.txt
  $ bower install
```
If you need `bower`, you can install it via`npm`: `npm install bower`.

For image import, you need the library pyexiv2, that is not available from PyPI.
However, it is contained in the Debian (and Arch Linux) package repositories.
You also need GExiv2.

## Licensing ##

This code is released under the GPLv3 (see `LICENSE`).
Contributions should have the same license.

This does not apply to the PhotoSwipe JavaScript library used, which is released under a modified MIT license (see http://photoswipe.com), with the following exception: "Do not create a public WordPress plugin based on it." Unfortunately, that makes PhotoSwipe non-free software. Patches replacing it by a free software solution (that works as well or better) are also welcome.
